//
//  ViewController.swift
//  GeeSwitch
//
//  Created by Dominik Brosch on 04.11.17.
//  Copyright © 2017 GeeSwitch. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func lamp1changed(_ sender: UISwitch) {
        // create the alert
        let alert = UIAlertController(title: "Status changed", message: "The lamp has been switched \(sender.isOn ? "on" : "off")", preferredStyle: UIAlertControllerStyle.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
}

